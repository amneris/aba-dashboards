class Dashing.AbaWidget extends Dashing.Widget

  @accessor 'status', Dashing.AnimatedValue
  @accessor 'color', Dashing.AnimatedValue

  constructor: ->
    super
    if @get('status')
      @renderStatus(@get('status'))
    if @get('color')
      @renderColor(@get('color'))

  onData: (data) ->
    if data.status
      @set('status', data.status)
      @renderStatus(@get('status'))
    if data.color
      @set('color', data.color)
      @renderColor(@get('color'))

  renderStatus: (status) ->
# clear existing "status-*" classes
    $(@get('node')).attr 'class', (i, c) ->
      c.replace /\bstatus-\S+/g, ''
    # add new class
    $(@get('node')).css('background-color', '');
    $(@get('node')).addClass "status-#{status}"

  renderColor: (color) ->
# clear existing "status-*" classes
    $(@get('node')).attr 'class', (i, c) ->
      c.replace /\bstatus-\S+/g, ''
    $(@get('node')).fadeOut().css('background-color', color).fadeIn()

  ready: ->
    detailUrl = @get('url')
    if detailUrl
      $span = $('<span>').addClass('widget-external-link')
      $i = $('<i>').addClass('fa').addClass('fa-external-link').css('color', 'white')
      $span.append($i)
      $span.click () ->
        window.open(detailUrl, "_blank");
        return false
      $(@get('node')).append($span)

