class Dashing.Meter extends Dashing.AbaWidget

  @accessor 'value', Dashing.AnimatedValue

  constructor: ->
    super
    @observe 'value', (value) ->
      $(@node).find(".meter").val(value).trigger('change')
      $(@node).find(".meter").val(value).trigger('configure',
        {
          "displayPrevious": true
        })

  ready: ->
    super
    meter = $(@node).find(".meter")
    meter.attr("data-bgcolor", meter.css("background-color"))
    meter.attr("data-fgcolor", meter.css("color"))
    meter.knob()
