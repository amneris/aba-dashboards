
# ABA English Dashboards

[Dashing](http://shopify.github.com/dashing) based information radiators (aka dashboards) for the ABA English software 
development team KPIs

Dashing is a Sinatra based dashboard framework written in Ruby and Coffeescript


## Getting Started

    Install the gem from the command line. Make sure you have Ruby 1.9+

    $ gem install dashing

    Clone the project

    $ git clone git@github.com:abaenglish/dashboards.git

    Change your directory to 'dashboards' and bundle gems

    $ bundle instal --path vendor/bundle

    Start the server!

    $ dashing start

    Point your browser at localhost:3030 and have fun!

The directory is setup as follows:

    Assets — All your images, fonts, and js/coffeescript libraries. Uses Sprockets
    Dashboards — One .erb file for each dashboard that contains the layout for the widgets.
    Jobs — Your ruby jobs for fetching data (e.g for calling third party APIs like twitter).
    Lib — Optional ruby files to help out your jobs.
    Public — Static files that you want to serve. A good place for a favicon or a custom 404 page.
    Widgets — All the html/css/coffee for individual widgets.

Run ```dashing``` from command line to find out what command line tools are available to you.


<a name="commit-msgs"></a>
## Git Commit Message Suggestions
* Consider starting the commit message with an applicable emoji:
    * :art: `:art:` when improving the format/structure of the code
    * :moyai: `:moyai:` when adding a new feature
    * :wrench: `:wrench:` when dealing with the toolchain (Git, Travis, etc)
    * :notebook: `:notebook` when dealing with docs
    * :racehorse: `:racehorse:` when improving performance
    * :penguin: `:penguin:` when fixing something on Linux
    * :apple: `:apple:` when fixing something on Mac OS
    * :bug: `:bug:` when fixing a bug
    * :bomb: `:bomb:` when removing code or files
    * :white_check_mark: `:white_check_mark:` when adding tests
    * :lock: `:lock:` when dealing with security
    * :arrow_up: `:arrow_up:` when upgrading dependencies
    * :arrow_down: `:arrow_down:` when downgrading dependencies
