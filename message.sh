#!/usr/bin/env bash


# curl -d '{ "auth_token": "HvD7DEcGFMMu83XB", "status": "danger", "title": "scheduled maintenance finishing", "text": "www.abaenglish.com is recovering. other systems may still have performance issues" }' \http://dashboards.aba.land:80/widgets/welcome
# curl -d '{ "auth_token": "HvD7DEcGFMMu83XB", "status": "danger", "title": "we are having DNS issues", "text": "systems may still have performance issues" }' \http://dashboards.aba.land:80/widgets/welcome
# curl -d '{ "auth_token": "HvD7DEcGFMMu83XB", "color": "#1e92cc", "title": "The scheduled maintenance has been completed.", "text": "\"Tracking & Attribution\" is experiencing issues. All the other systems are operational." }' \http://dashboards.aba.land:80/widgets/welcome

# curl -d '{ "auth_token": "HvD7DEcGFMMu83XB", "color": "#1e92cc", "title": "updating the slave database", "text": "intranet is down, BI  may have performance issues, All the other systems are operational." }' \http://dashboards.aba.land:80/widgets/welcome
# curl -d '{ "auth_token": "HvD7DEcGFMMu83XB", "color": "green", "title": "slave upgrade done!", "text": "All systems are operational." }' \http://dashboards.aba.land:80/widgets/welcome

# curl -d '{ "auth_token": "HvD7DEcGFMMu83XB", "color": "#1e92cc", "title": "campus 4.113.0 and course 4.20.0", "text": "New versions of campus and course have been deployed to the production environment." }' \http://dashboards.aba.land:80/widgets/welcome

# curl -d '{ "auth_token": "HvD7DEcGFMMu83XB", "status": "danger", "title": "www.abaenglish.com is having performance issues", "text": "f*5g Nexica and f*5g NFS" }' \http://dashboards.aba.land:80/widgets/welcome

# curl -d '{ "auth_token": "HvD7DEcGFMMu83XB", "status": "danger", "title": "login in Android is not working", "text": "investigating..." }' \http://dashboards.aba.land:80/widgets/welcome

# curl -d '{ "auth_token": "HvD7DEcGFMMu83XB", "color": "#1e92cc", "title": "www.abaenglish.com was having issues", "text": "Looks like everything is working. We are watching it." }' \http://dashboards.aba.land:80/widgets/welcome

# curl -d '{ "auth_token": "HvD7DEcGFMMu83XB",  "current": "7", "status": "ok", "difference": " 6", "moreinfo":"alejandro" , "arrow": "icon-arrow-up"}' \http://localhost:3030/widgets/leads

# curl -d '{ "auth_token": "HvD7DEcGFMMu83XB",  "suffix": "\uf007", "moreinfo":"new users today and 1h deviation from SPLW"}' \http://localhost:3030/widgets/leads

# curl -d '{ "auth_token": "HvD7DEcGFMMu83XB", "items": [{ "label": "slave4", "value": "1" }] }' \http://localhost:3030/widgets/slaves

# curl -d '{ "auth_token": "HvD7DEcGFMMu83XB", "items": [{ "label": "abawebapps", "value": "4.126.2" },{ "label": "www", "value": "1.13.0" },{ "label": "android", "value": "2.1.2" },{ "label": "iOS", "value": "2.1.2" }] }' \http://dashboards.aba.land:80/widgets/releases

# curl -d '{ "auth_token": "HvD7DEcGFMMu83XB", "title": "", "text": "--" }' \http://dashboards.aba.land:80/widgets/welcome

# curl -d '{ "auth_token": "HvD7DEcGFMMu83XB", "color": "green", "title": "Everything 200 OK", "text": "All systems are operational." }' \http://dashboards.aba.land:80/widgets/welcome

# curl -d '{ "auth_token": "HvD7DEcGFMMu83XB", "color": "#1e92cc", "title": "new DB server running!", "text": "Everything 200 OK, All systems are operational." }' \http://dashboards.aba.land:80/widgets/welcome

# curl -d '{ "auth_token": "HvD7DEcGFMMu83XB", "status": "danger", "title": "investigating...", "text": "Return code of 127 is out of bounds. Make sure the plugin you are trying to run actually exists." }' \http://dashboards.aba.land:80/widgets/welcome

# curl -d '{ "auth_token": "HvD7DEcGFMMu83XB", "value": "50", "fgcolor": "black", "bgcolor": "yellow" }' \http://localhost:3030/widgets/dbcampus

# curl -d '{ "auth_token": "HvD7DEcGFMMu83XB",  "color": "#cc33ff", "text": "6.989.38x y contando...", "title":"llegando a los 7MM" }' \http://dashboards.aba.land:80/widgets/welcome

# curl -d '{ "auth_token": "HvD7DEcGFMMu83XB", "color": "green", "title": "Everything 200 OK", "text": "All systems are operational." }' \http://dashboards.aba.land:80/widgets/welcome

# curl -d '{ "auth_token": "HvD7DEcGFMMu83XB",  "status":"ok"}' \http://localhost:3030/widgets/dbcampus

# curl -d '{ "auth_token": "HvD7DEcGFMMu83XB", "color": "green", "title": "Everything 200 OK", "text": "All systems are operational." }' \http://localhost:3030/widgets/welcome

# curl -d '{ "auth_token": "HvD7DEcGFMMu83XB", "status": "ok" }' \http://dashboards.aba.land:80/widgets/welcome
# curl -d '{ "auth_token": "HvD7DEcGFMMu83XB", "color": "blue" }' \http://dashboards.aba.land:80/widgets/welcome

# curl -d '{ "auth_token": "HvD7DEcGFMMu83XB",  "comments": [{ "name": "ascandroli", "body": "este es el texto del tweet que también puede llegar a ser bastante largo", "avatar": "https://pbs.twimg.com/profile_images/1272859847/mate_normal.png" }]}' \http://localhost:3030/widgets/twitter_mentions

# curl -d '{ "auth_token": "HvD7DEcGFMMu83XB",  "since_date": "2016-02-23"}' \http://dashboards.aba.land:80/widgets/time_since_last

# curl -d '{ "auth_token": "HvD7DEcGFMMu83XB", "color": "green", "title": "Everything 200 OK", "text": "All systems are operational." }' \http://localhost:3030/widgets/welcome

# curl -d '{ "auth_token": "HvD7DEcGFMMu83XB", "color": "green", "title": "Everything 200 OK", "text": "All systems are operational." }' \http://dashboards.aba.land:80/widgets/welcome

# dashboard lastincident since_date:"Sep 13 2016 08:33:37 GMT+0200 (CEST)"
