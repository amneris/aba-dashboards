require 'dashing'

configure do
  set :auth_token, ENV['AUTH_TOKEN']

  # Please configure your Icinga access here
  set :icinga_cgi, 'https://monitoritzacio.nexica.com/icinga/cgi-bin/status.cgi'
  set :icinga_user, 'abaenglish'
  set :icinga_pass, ENV['ICINGA_PASS']

  set :newrelic_api_key, ENV['NEWRELIC_API_KEY']
  set :newrelic_query_key, ENV['NEWRELIC_QUERY_KEY']

  set :aws_access_key_id, ENV['AWS_ACCESS_KEY_ID']
  set :aws_secret_access_key, ENV['AWS_SECRET_ACCESS_KEY']

  helpers do

    def protected!
      unless authorized? || whitelisted?
        response['WWW-Authenticate'] = %(Basic realm="Restricted Area")
        throw(:halt, [401, "Not authorized\n"])
      end
    end

    def authorized?
      @auth ||= Rack::Auth::Basic::Request.new(request.env)
      @auth.provided? && @auth.basic? && @auth.credentials && @auth.credentials == ['abaenglish', ENV['AUTH_PASS']]
    end

    def whitelisted?
      req = Rack::Request.new(request.env)
      ['91.126.242.146', '127.0.0.1'].include?(req.ip)
    end
  end
end

map Sinatra::Application.assets_prefix do
  run Sinatra::Application.sprockets
end

run Sinatra::Application
