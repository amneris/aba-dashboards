require 'net/http'

# WARN: beacuse Net::HTTP.get doesn't follow redirects nicely
# WARN: use /api/public/card/{ID}/query/csv instead of /public/question/{ID}.csv
uri = "http://metabase.aba.solutions/api/public/card/4f34afc6-c649-49a5-a621-1fb0da428155/query/csv"

#Id of the widget
id = "refunds"

SCHEDULER.every '90s', :first_in => 0 do |job|

    #This is for when there is no proxy
    res = Net::HTTP.get(URI(uri))

    lines = res.split("\n")

    tickets = lines[1].to_i

    send_event(id, { current: tickets})

end