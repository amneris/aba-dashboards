require "net/https"
require "uri"
require "json"

$url = "https://insights-api.newrelic.com/v1/accounts/1268965/query"
$last_incident_query = 'SELECT latest(timestamp) FROM alert FACET policy_name, dateOf(timestamp) SINCE this year LIMIT 1'

def days_since_date (date)
  diff_in_seconds = Time.new().to_time - DateTime.parse(date).to_time
  puts ("date since last incident: " + date + " That was " + diff_in_seconds.to_s + " seconds ago")
  return diff_in_seconds.to_i / (60 * 60 * 24)  
end

def get_days_since_last_incident
  uri = URI.parse($url)
  http = Net::HTTP.new(uri.host, uri.port)
  http.use_ssl = true
  
  result = nrql(http, $last_incident_query, uri)

  return days_since_date(result["facets"][0]["name"][1])   
end

#days since last incident
SCHEDULER.every '60s', :first_in => 0 do |job|

  days = get_days_since_last_incident()

  if days == 0
    status = 'danger'  
  elsif days == 1
    status = 'warning'
  else
    status = 'ok'  
  end
    
  send_event("time_since_last", {current: days, status: status})  
  puts("days since last incident " + days.to_s + ". Status " + status)
end