#!/usr/bin/env ruby

#jobs/ec2.rb

# copied from https://gist.github.com/jwalton/6614087

require './lib/dashing_ec2'

puts settings.inspect

$dashing_ec2 = nil


def init()
  $dashing_ec2 ||= DashingEC2.new({
                                      access_key_id: settings.aws_access_key_id,
                                      secret_access_key: settings.aws_secret_access_key
                                  })
end

# See documentation here for cloud watch API here: https://github.com/aws/aws-sdk-ruby/blob/af638994bb7d01a8fd0f8a6d6357567968638100/lib/aws/cloud_watch/client.rb
# See documentation on various metrics and dimensions here: http://docs.aws.amazon.com/AWSEC2/2011-07-15/UserGuide/index.html?using-cloudwatch.html

# Note that Amazon charges [$0.01 per 1000 reqeuests](http://aws.amazon.com/pricing/cloudwatch/),
# so:
#
# | frequency | $/month/stat |
# |:---------:|:------------:|
# |     1m    |     $0.432   |
# |    10m    |     $0.043   |
# |     1h    |     $0.007   |
#
# In the free tier, stats are only available for 5m intervals, so querying more often than
# once every 5 minutes is kind of pointless.  You've been warned. :)
#
SCHEDULER.every '10m', :first_in => 0 do |job|

  init
  reservations = $dashing_ec2.describe_instances('eu-west-1', true)
  nInstances_w_high_cpu = 0
  reservations.each do |reservation|
    reservation.instances.each do |instance|      
      cpu_data = $dashing_ec2.getInstanceStats(instance.instance_id,
                                             "eu-west-1",
                                             "CPUUtilization",
                                             :average,
                                             {
                                                 start_time: Time.now - 600,
                                                 end_time: Time.now,
                                                 period: 60,
                                             },'EC2')
      
      cpu_percentage = cpu_data[:data].last[:y].to_f.round(1)
      if (cpu_percentage > 20)
        puts (instance.instance_id + " CPU " + cpu_percentage.to_s + "%")
        nInstances_w_high_cpu = nInstances_w_high_cpu + 1
      end #if
    end #instance loop
  end #reservations loop
  send_event "high_cpu", {current: nInstances_w_high_cpu}
end # SCHEDULER
