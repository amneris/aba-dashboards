require 'net/http'

# WARN: beacuse Net::HTTP.get doesn't follow redirects nicely
# WARN: use /api/public/card/{ID}/query/csv instead of /public/question/{ID}.csv
# for redirects check https://shadow-file.blogspot.com.es/2009/03/handling-http-redirection-in-ruby.html
uri = "http://metabase.aba.solutions/api/public/card/549813f3-24de-44c3-9795-478aac205dd1/query/csv"

#Id of the widget
id = "leads"

SCHEDULER.every '300s', :first_in => 0 do |job|

    #This is for when there is no proxy
    res = Net::HTTP.get(URI(uri))

    rows = res.split("\n")

    columns = rows[1].split(",")

    today = columns[0].to_i
    _1d_devn = columns[4].to_f
    _7d_devn = columns[5].to_f

    status = 'unknown'

    if _1d_devn >= -0.1 &&  _7d_devn >= -0.1
      status = 'ok'
    end

    if _1d_devn < -0.1 || _7d_devn < -0.1
        status = 'danger'
        if _1d_devn < -0.25 || _7d_devn < -0.25
            status = 'warning'
        end
    end

    arrow = _7d_devn > 0 ? 'icon-arrow-up' : 'icon-arrow-down'
    diff = Integer(_7d_devn * 100)

    send_event(id, {current: today, status: status, difference: " #{diff}%" , arrow: arrow})

end