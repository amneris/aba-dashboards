require "net/https"
require "uri"

$url = "https://insights-api.newrelic.com/v1/accounts/1268965/query"
$total_monitors_query = 'SELECT uniqueCount(monitorId) FROM SyntheticCheck SINCE 1 DAY AGO'
$failing_monitors_query = 'SELECT uniqueCount(monitorId) FROM SyntheticCheck SINCE 1 minute AGO where result = \'FAILED\''
$numberpodsproduction = 'FROM K8sNodeSample, K8sPodSample select uniqueCount(podName) as Pods WHERE namespace != \'kube-system\''

def nrql(http, totalMonitorsQuery, uri)

  params = {:nrql => totalMonitorsQuery}
  uri.query = URI.encode_www_form(params)
  req = Net::HTTP::Get.new(uri.request_uri)
  req['X-Query-Key'] = settings.newrelic_query_key
  res = http.request(req)
  json = JSON.parse(res.body)
  #puts (json)
  if (json.key?("results"))
    return json["results"].first["uniqueCount"]
  else
    return json
  end
end

def update_synthetics(http, uri)

  failing = nrql(http, $failing_monitors_query, uri)
  current = nrql(http, $total_monitors_query, uri)

  status = 'ok'
  moreinfo = 'monitors'

  if failing > 0
    current = failing
    status = 'danger'
    moreinfo = 'failing monitors'
    the_time = Time.new()
    send_event('lastincident', { time: the_time.to_i })
  else

  end

  send_event("synthetics", {current: current, status: status, moreinfo: moreinfo})

end

def update_numberpodsproduction(http, uri)

  current = nrql(http, $numberpodsproduction, uri)
  status = 'ok'

  if current <= 0
    status = 'danger'
  end

  send_event("numberpodsproduction", {current: current, status: status})

end

def days_since_last_incident()
 
  send_event('dayslastalert', { value: 2})
 
end  

SCHEDULER.every '60s', :first_in => 0 do |job|

#  res = Net::HTTP.get(URI(uri))

  uri = URI.parse($url)
  http = Net::HTTP.new(uri.host, uri.port)
  http.use_ssl = true

  #update_synthetics(http, uri)
  days_since_last_incident  
  update_numberpodsproduction(http, uri)


end
