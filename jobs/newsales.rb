require 'net/http'

# WARN: beacuse Net::HTTP.get doesn't follow redirects nicely
# WARN: use /api/public/card/{ID}/query/csv instead of /public/question/{ID}.csv
uri = "http://metabase.aba.solutions/api/public/card/86cdfa39-0ad6-42f0-8d24-b96a7febe142/query/csv"

#Id of the widget
id = "newsales"

SCHEDULER.every '90s', :first_in => 0 do |job|

    #This is for when there is no proxy
    res = Net::HTTP.get(URI(uri))

    lines = res.split("\n")

    tickets = lines[1].to_i

    send_event(id, { current: tickets})

end