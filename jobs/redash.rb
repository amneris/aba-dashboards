require 'net/http'

# WARN: beacuse Net::HTTP.get doesn't follow redirects nicely
# WARN: use /api/public/card/{ID}/query/csv instead of /public/question/{ID}.csv
# for redirects check https://shadow-file.blogspot.com.es/2009/03/handling-http-redirection-in-ruby.html


def  send_data(id, uri)
  #This is for when there is no proxy
  res = Net::HTTP.get(URI(uri))
  lines = res.split("\n")
  if id == "android_rate" or id == "ios_rate"
    tickets = lines[1].to_f.round(2)
    send_event(id, { current: tickets })

  else
    tickets = lines[1].to_i
    send_event(id, { current: tickets })
  end

end

  SCHEDULER.every '90s', :first_in => 0 do |job|
  send_data("leads", "http://analytics.aba.solutions/api/queries/229/results.csv?api_key=XmrPGJ6JSpvq87cKwGmLJkr8wTR80u17PIltMQhP")
  send_data("refunds", "http://redash.analytics.aba.solutions/api/queries/243/results.csv?api_key=3iXOyzMiGLtbfWFSSfaRvDl25hfPCdBt9c9dUae8")
  send_data("revenue", "http://redash.analytics.aba.solutions/api/queries/246/results.csv?api_key=DmxaHnseVxxv8ocE5L2oReGUf9MPWNI8EZA6cX7l")
  send_data("first_sales", "http://redash.analytics.aba.solutions/api/queries/244/results.csv?api_key=2p1XeHrcukI7virlf7CzJbRe9ngTgNZj8wI364Y6")
  send_data("android_rate", "http://redash.analytics.aba.solutions/api/queries/248/results.csv?api_key=PmPUQkzEm9qo5gREwjYMkKWI2Hphq5uSAty3wSZ8")
  send_data("ios_rate", "http://redash.analytics.aba.solutions/api/queries/247/results.csv?api_key=yXWDTzjsRyI9rnjrwYSj4Z6qsYOkqhrMha1PzKzg")
  send_data("next_sales", "http://redash.analytics.aba.solutions/api/queries/245/results.csv?api_key=dDHukgAiiARledWAFEvSJZJsdX9HRkQNh6yEwREs")
  end
