require 'net/http'

# WARN: beacuse Net::HTTP.get doesn't follow redirects nicely
# WARN: use /api/public/card/{ID}/query/csv instead of /public/question/{ID}.csv
uri = "http://metabase.aba.solutions/api/public/card/b3585b07-2c76-42a6-b304-0e6c254659ca/query/csv"

#Id of the widget
id = "d0sales"

SCHEDULER.every '90s', :first_in => 0 do |job|

    #This is for when there is no proxy
    res = Net::HTTP.get(URI(uri))

    lines = res.split("\n")

    tickets = lines[1].to_i

    send_event(id, { current: tickets})

end