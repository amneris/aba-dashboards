#!/usr/bin/env ruby

#jobs/ec2.rb

# copied from https://gist.github.com/jwalton/6614087

require './lib/dashing_ec2'

puts settings.inspect

$dashing_ec2 = nil


def init()
  $dashing_ec2 ||= DashingEC2.new({
                                      access_key_id: settings.aws_access_key_id,
                                      secret_access_key: settings.aws_secret_access_key
                                  })
end

# See documentation here for cloud watch API here: https://github.com/aws/aws-sdk-ruby/blob/af638994bb7d01a8fd0f8a6d6357567968638100/lib/aws/cloud_watch/client.rb
# See documentation on various metrics and dimensions here: http://docs.aws.amazon.com/AWSEC2/2011-07-15/UserGuide/index.html?using-cloudwatch.html

# Note that Amazon charges [$0.01 per 1000 reqeuests](http://aws.amazon.com/pricing/cloudwatch/),
# so:
#
# | frequency | $/month/stat |
# |:---------:|:------------:|
# |     1m    |     $0.432   |
# |    10m    |     $0.043   |
# |     1h    |     $0.007   |
#
# In the free tier, stats are only available for 5m intervals, so querying more often than
# once every 5 minutes is kind of pointless.  You've been warned. :)
#
SCHEDULER.every '1m', :first_in => 0 do |job|

  init

  cpu_usage = [
      {name: 'rds-dbcampus-2', instance_id: "rds-dbcampus-2", region: 'eu-west-1'},
      {name: 'rds-dbcampus-1', instance_id: "rds-dbcampus-1", region: 'eu-west-1'},
      {name: 'db-services-pro', instance_id: "db-services-pro", region: 'eu-west-1'},
      {name: 'db-services-pro-slave1', instance_id: "db-services-pro-slave1", region: 'eu-west-1'}
  ]

  cpu_series = []
  cpu_usage.each do |item|
    cpu_data = $dashing_ec2.getInstanceStats(item[:instance_id],
                                             item[:region],
                                             "CPUUtilization",
                                             :average,
                                             {
                                                 start_time: Time.now - 300,
                                                 end_time: Time.now,
                                                 period: 60,
                                             })
    cpu_data[:name] = item[:name]
    cpu_series.push cpu_data
  end

  # If you're using the Rickshaw Graph widget: https://gist.github.com/jwalton/6614023
  # send_event "ec2-cpu", { series: cpu_series }

  # If you're just using the regular Dashing graph widget:
  # send_event "dbcampus", { points: cpu_series[0][:data] }
  # send_event "replicas", { points: cpu_series[1][:data] }

  send_event "dbcampus", {value: cpu_series[0][:data].last[:y].to_i}
  send_event "dbcampus2", {value: cpu_series[1][:data].last[:y].to_i}
  send_event "dbservices", {value: cpu_series[2][:data].last[:y].to_i}
  send_event "dbservices2", {value: cpu_series[3][:data].last[:y].to_i}

end # SCHEDULER

SCHEDULER.every '1m', :first_in => 0 do |job|
  init
  send_event "ec2instances", {current: $dashing_ec2.describe_instances('eu-west-1')}
end
