require 'google/api_client'
require 'date'

# Update these to match your own apps credentials
service_account_email = '213668991404-k0t9vin2redilu8rs8k4bu8unorh8epg@developer.gserviceaccount.com'
key_file = 'abaenglish-com-3abebb411741.p12' # File containing your private key
key_secret = 'notasecret' # Password to unlock private key

# Get the Google API client
client = Google::APIClient.new(
  :application_name => 'Dashing Widget',
  :application_version => '0.01'
)

android = []
blog = []
campus = []
landings = []
ios = []
www = []

last_x = 100

# Load your credentials for the service account
key = Google::APIClient::KeyUtils.load_from_pkcs12(key_file, key_secret)
client.authorization = Signet::OAuth2::Client.new(
  :token_credential_uri => 'https://accounts.google.com/o/oauth2/token',
  :audience => 'https://accounts.google.com/o/oauth2/token',
  :scope => 'https://www.googleapis.com/auth/analytics.readonly',
  :issuer => service_account_email,
  :signing_key => key)

def fill_array_with_points(array)
  (1..100).each do |i|
    array << { x: i, y: 0 }
  end
end

fill_array_with_points(android)
fill_array_with_points(blog)
fill_array_with_points(campus)
fill_array_with_points(landings)
fill_array_with_points(ios)
fill_array_with_points(www)

def method_name(client, analytics, data_id, profile_id, values, last_x)
  # Execute the query
  response = client.execute(:api_method => analytics.data.realtime.get, :parameters => {
                                                                          'ids' => "ga:" + profile_id,
                                                                          'metrics' => "ga:activeVisitors",
                                                                      })

  values.shift
  values << { x: last_x, y: response.data.rows[0][0].to_i }

  # Update the dashboard
  #puts response.data.inspect
  send_event(data_id, points: values)

end


# Start the scheduler
SCHEDULER.every '30s', :first_in => 0 do

  # Request a token for our service account
  client.authorization.fetch_access_token!

  # Get the analytics API
  analytics = client.discovered_api('analytics','v3')

  last_x += 1

  method_name(client, analytics, 'active_users_android', '122263844', android, last_x)
  method_name(client, analytics, 'active_users_blog', '130164649', blog, last_x)
  method_name(client, analytics, 'active_users_campus', '70039636', campus, last_x)
  method_name(client, analytics, 'active_users_landings', '133518042', landings, last_x)
  method_name(client, analytics, 'active_users_ios', '122242394', ios, last_x)
  method_name(client, analytics, 'active_users_www', '70030949', www, last_x)



end