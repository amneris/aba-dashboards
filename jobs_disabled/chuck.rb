require 'net/http'
require 'json'

#The Internet Chuck Norris Database
server = "http://api.icndb.com"

#Id of the widget
id = "welcome"

teammembers = ['@ascandroli','@gnatok','@amora', '@akorotkov', '@carles', '@dani', '@earandes', '@imorato', '@jesus_espejo', '@jmerchan', '@mikkel', '@rfrances']

SCHEDULER.every '1d', :first_in => '5m' do |job|
  random_member = teammembers.sample

  #The uri to call, swapping in the team members name
  uri = URI("#{server}/jokes/random?firstName=#{random_member}&lastName=&limitTo=[nerdy]&escape=javascript")

  #This is for when there is no proxy
  res = Net::HTTP.get(uri)

  #marshal the json into an object
  j = JSON[res]

  #Get the joke
  joke = j['value']['joke']

  #Send the joke to the text widget
  send_event(id, { title: "#{random_member} facts", text: joke })

end