require 'net/http'
require 'json'
require 'optparse'

#
# @host: something like www.foobar.com
# @path: something like /server-status
#
# The request will be something like http://host/path?auto
#
def get_status(host, path="/server-status")
=begin
    req = Net::HTTP.new(host, 80)
    headers, data = req.get(path+"?auto")
=end

    data = Net::HTTP.get(URI('http://apachemonitor2/server-status?auto'))

#    return "HTTP/#{headers.http_version} #{headers.code}" if headers.code != "200"

    lines = data.split("\n")
    lines.find { |line| line =~ /ReqPerSec/ }.match(/^ReqPerSec: (.*)$/)
    req_per_sec = $1.dup
    perfdata = "req_per_sec=#{req_per_sec}"

    lines.find { |line| line =~ /BusyWorkers/ }.match(/^BusyWorkers: (.*)$/)
    busy_workers = $1.dup
    perfdata << " busy_workers=#{busy_workers}"

    lines.find { |line| line =~ /CPULoad/ }.match(/^CPULoad: (.*)$/)
    cpu_load = $1.dup
    perfdata << " cpu_load=#{cpu_load}"

    lines.find { |line| line =~ /Total Accesses/ }.match(/^Total Accesses: (.*)$/)
    total_accesses = $1.dup
    perfdata << " total_accesses=#{total_accesses}"

    lines.find { |line| line =~ /Uptime/ }.match(/^Uptime: (.*)$/)
    uptime = $1.dup
    perfdata << " uptime=#{uptime}"

    lines.find { |line| line =~ /IdleWorkers/ }.match(/^IdleWorkers: (.*)$/)
    idle_workers = $1.dup
    perfdata << " idle_workers=#{idle_workers}"

    lines.find { |line| line =~ /BytesPerSec/ }.match(/^BytesPerSec: (.*)$/)
    bytes_per_sec = $1.dup
    perfdata << " bytes_per_sec=#{bytes_per_sec}"

    return cpu_load
end

SCHEDULER.every '5s', :first_in => 0 do |job|

    retval = get_status('apachemonitor2')
    puts retval

   send_event('cpus', { value: retval.to_i, color: 'green'})

end