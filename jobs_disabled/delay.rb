require 'net/http'

uri = "http://bi.aba.land/report.php?r=22&m=7&h=a39484fbcba8383292aa1dc1cc84272e3bacfbae&i=1&export=csv"

#Id of the widget
id = "delay"

SCHEDULER.every '90s', :first_in => 0 do |job|

    #This is for when there is no proxy
    res = Net::HTTP.get(URI(uri))

    rows = res.split("\n")

    columns = rows[1].split(",")

    hours = columns[0].to_i

    status = 'unknown'

    if hours <= 12
      status = 'ok'
    end

    if hours > 12
        status = 'danger'
        if hours > 24
            status = 'warning'
        end
    end

    send_event(id, {current: hours, status: status})

end