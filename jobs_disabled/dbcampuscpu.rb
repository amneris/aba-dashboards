require 'net/http'
require 'uri'

#Id of the widget
id = "dbcampus"

def removeQuotes(quotedValue)
    n = quotedValue.size
    quotedValue[1..n-1]
end

SCHEDULER.every '180s', :first_in => 0 do |job|

    uri = URI.parse("https://performance.nexica.net/index.php")
    http = Net::HTTP.new(uri.host, uri.port)
    http.use_ssl = true
    http.verify_mode = OpenSSL::SSL::VERIFY_NONE

    req = Net::HTTP::Post.new(uri.request_uri)
    req.set_form_data({
                          "action" => "login",
                          "login_username" => "abaenglish",
                          "login_password" => "Ab4En!!",
                          "realm" => "local"

                      })
    res = http.request(req)
    cookie = res['Set-Cookie']

    uri = URI.parse("https://performance.nexica.net/cacti/graph_xport.php?local_graph_id=103337&rra_id=8")
    http = Net::HTTP.new(uri.host, uri.port)
    http.use_ssl = true
    http.verify_mode = OpenSSL::SSL::VERIFY_NONE
    req = Net::HTTP::Get.new(uri.request_uri)
    req['cookie'] = cookie
    res = http.request(req)

    rows = res.body.split("\n")
    updated_at = 0
    i = 1
    value = 0.0
    cpus = 4
    while value <= 0.0 and i < rows.size
        row = rows[rows.size - i].split(",")
        updated_at = DateTime.strptime(removeQuotes(row[0]), '%Y-%m-%d %H:%M:%S').to_time.to_i - (60 * 60) # adjust for TIME ZONE
        value = removeQuotes(row[row.size - 1]).to_f
        cpus = row.size - 2 # the first column is the date, the last column is the total, the rest of the columns are the CPUs.
        i += 1
    end

    status = 'ok'
    value = (value / cpus).round

    if value > 80
        status = 'danger'
        if value > 90
            status = 'warning'
        end
    end

    if Time.now.to_i - updated_at > 60 * 15 # greater than 15 minutes
        status = 'unknown'
    end

    send_event(id, {value: value, updatedAt: updated_at, status: status})

end